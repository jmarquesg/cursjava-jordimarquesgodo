use vgames;

select count(*) from fullcsv;
select platform,count(platform) from fullcsv group by platform;
select count(distinct platform) from fullcsv;
select sum(global_sales) from fullcsv where platform = "PSP";
select sum(eu_sales) as Europa, sum(jp_sales) as Japón, sum(na_sales) as Norte_America from fullcsv; 
select sum(eu_sales)/741.4 as Europa_capita, sum(jp_sales)/126.5 as Japón_capita, sum(na_sales)/482 as Norte_America_capita from fullcsv;
select sum(global_sales), platform from fullcsv group by platform;
select sum(global_sales),platform from fullcsv where year_of_release in("2008","2009","2010") group by platform;
select sum(global_sales),year_of_release from fullcsv where genre="Sports" group by year_of_release;
select sum(global_sales),year_of_release from fullcsv where genre="Strategy" group by year_of_release;
select sum(eu_sales) as Europa, sum(jp_sales) as Japón, sum(na_sales) as Norte_America , genre
from fullcsv 
where genre in ("racing","sports","simulation")
group by genre;

select name as titulo, genre as nombre_de_genero ,sum(global_sales) as ventes
from fullcsv
group by name
order by global_sales desc
limit 10;

select name as titulo, genre as nombre_de_genero, year_of_release as any_de_mes_ventes, max(global_sales) as ventes
from fullcsv
where genre = "Strategy";

select platform as consola, sum(global_sales) as ventes
from fullcsv
where year_of_release in (2010,2011,2012,2013,2014,2015)
group by platform;


select name as titol
from fullcsv
where name like "%barbie%";


select
 CASE
    WHEN name LIKE '%theft%' THEN 'GTA'
    WHEN name LIKE '%Mario%' THEN "Mario"
    WHEN name LIKE "%Barbie%" THEN "Barbie"
    WHEN name LIKE "%Poke%" THEN "Pokemon"
  END AS titol, sum(global_sales) as ventes
from fullcsv
group by 
CASE
    WHEN name LIKE '%theft%'   THEN "GTA"
    WHEN name LIKE '%Mario%' THEN "Mario"
    WHEN name LIKE "%Barbie%" THEN "Barbie"
    WHEN name LIKE "%Poke%" THEN "Pokemon"
  END
  order by ventes desc;