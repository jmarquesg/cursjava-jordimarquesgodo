package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class AlumnoController {
  
    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "alumnos";
	private static final String KEY = "idalumnos";


    //getAll JSOn devuelve getAll en formato JSON
    // OJO! ver dependencia maven con <groupId>com.google.code.gson</groupId>

    public static String getAllJSON(){
        List<Alumno> listaAlumnos = AlumnoController.getAll();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        //Gson gson = new Gson();
        return gson.toJson(listaAlumnos);
    }

    // getAll devuelve todos los registros de la tabla
    public static List<Alumno> getAll(){
        
        List<Alumno> listaAlumnos = new ArrayList<Alumno>();
		String sql = String.format("select %s,nombre,email,telefono, alta, urlfoto from %s", KEY, TABLE);
		//String sql = "select id,nombre,password from "+TABLE;
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {


			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Alumno u = new Alumno(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getString("email"),
                    rs.getString("telefono"),
                    rs.getDate("alta"),
                    rs.getString("urlfoto")
                );
                listaAlumnos.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaAlumnos;

    }

    //getId devuelve un registro
    public static Alumno getId(int id){
        Alumno u = null;
        String sql = String.format("select %s,nombre,email,telefono, alta, urlfoto from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Alumno(
                rs.getInt(KEY),
                rs.getString("nombre"),
                rs.getString("email"),
                rs.getString("telefono"),
                rs.getDate("alta"),
                rs.getString("urlfoto")
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Alumno al) {
        String sql;
        if (al.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?, email=?, telefono=?, alta=? where %s=%d", TABLE, KEY, al.getId());
        }else {
            sql = String.format("INSERT INTO %s (nombre, email, telefono, alta) VALUES (?,?,?,?)", TABLE);
        }

        // sql para introducir password
        String sql2 = String.format("update %s set clave=password(?)  where %s=?", TABLE, KEY);
                   
        try (Connection conn = DBConn.getConn();
        PreparedStatement pstmt = conn.prepareStatement(sql);
        PreparedStatement pstmt2 = conn.prepareStatement(sql2);
        Statement stmt = conn.createStatement()) {
            pstmt.setString(1, al.getNombre());
            pstmt.setString(2, al.getEmail());
            pstmt.setString(3, al.getTelefono());

            // ojo! la clase que devuelve getAlta es un java.util.Date
            // en cambio, el método setDate espera una fecha de tipo java.sql.Date
            // tenemos que hacer la conversión de una a otra, la forma más fácil es convertir
            // la primera en milisegundos y crear un objeto de la segunda a partir de éstos
            long ms = al.getAlta().getTime();
            java.sql.Date fecha_sql = new java.sql.Date(ms);
            pstmt.setDate(4, fecha_sql);
            
            System.out.println("hola "+al.getNombre());
                  
            pstmt.executeUpdate();

        if (al.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    al.setId(rs.getInt(1));
                    //actualizamos campo clave!
                    // establecemos valor para clave qyue será encriptado mediante password(x)
                    pstmt2.setString(1, al.getClave());
                    //establecemos id
                    pstmt2.setInt(2, al.getId());
                    pstmt2.executeUpdate();
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    // asignaFoto asigna URLde FOTO a alumno por id
    public static void setUrlfoto(int id, String url){
        String sql = String.format("UPDATE %s SET urlfoto=? where %s=?", TABLE, KEY);
        try (Connection conn = DBConn.getConn();
        PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, url);
            pstmt.setInt(2, id);
            pstmt.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // asignaFoto asigna URLde FOTO a alumno por id
    public static String getUrlfoto(int id){
        String sql = String.format("SELECT urlfoto FROM %s where %s=%d", TABLE, KEY, id);
        String resp = "";
        try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                resp = rs.getString(1);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return resp;
    }
        


    
    public static int checkLogin(String nombre, String password){
        int resId = -1;
        
        String sql = String.format("select %s from %s where email=? and clave=password(?)", KEY, TABLE);
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
            pstmt.setString(1, nombre);
            pstmt.setString(2, password);

            ResultSet rs = pstmt.executeQuery();
        
            if (rs.next()) {
                    resId = rs.getInt(1);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return resId;
    }
    

}


