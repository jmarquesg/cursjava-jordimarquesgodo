package com.sjava.web;

public class Curso {

    private int id;
    private String nombre;
    private int horas;
    private int idProfesor;

    public Curso(String nombre, int horas, int idProfesor) {
        this.nombre = nombre;
        this.horas = horas;
        this.idProfesor = idProfesor;
    }

    public Curso(int id, String nombre, int horas, int idProfesor) {
        this.id = id;
        this.nombre = nombre;
        this.horas = horas;
        this.idProfesor = idProfesor;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public int getHoras(){
        return this.horas;
    }

    protected void setHoras(int horas){
        this.horas = horas;
    }

    public int getIdProfesor(){
        return this.idProfesor;
    }
    
    protected void setIdProfesor(int idProfesor){
        this.idProfesor = idProfesor;
    }


    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }



}