<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>

<%
    String id = request.getParameter("id");
    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/academiapp/index.jsp");
    }else{

        int id_numerico = Integer.parseInt(id);
        ProfesorController.removeId(id_numerico);
        response.sendRedirect("/academiapp/profesor/list.jsp");

    }


%>
