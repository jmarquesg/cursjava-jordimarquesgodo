<%@page contentType="text/html;charset=UTF-8" %>

<%
/*
LOGIN.JSP - Módulo "incrustable"
contiene menú modal donde se solicta el nombre de usuario o email y el password
el form envía via post el resultado al JSP el codigo siguiente procesa los datos e intenta el login
en caso que lo consiga, asigna las variables de sesión correspondientes
este LOGIN.JSP debe incrustarse ANTES del MENU.JSP
*/

System.out.println(request.getContextPath());

// si recibimos datos via POST
if ("POST".equalsIgnoreCase(request.getMethod())) {
    request.setCharacterEncoding("UTF-8");

    //miramos si existe el parámetro "loginpost"
    String login = request.getParameter("loginpost");
    if (login!=null){
        String paramName = request.getParameter("loginname");
        String paramPassword = request.getParameter("loginpassword");
        //aquí ejecutamos el método que debe verificar nombre y password
        //recibiremos en itemId un id válido >0 o bien -1 (VER METODO)
        int itemId = AlumnoController.checkLogin(paramName,paramPassword);
        //si hemos recibido id válido, asignamos variables de session
        if (itemId>0) {
            //HttpSession session=request.getSession();
            session.setAttribute("loginName",paramName);
            session.setAttribute("loginId",itemId);
        }
    }

    //miramos si existe el parámetro "logoutpost"
    String logout = request.getParameter("logoutpost");
    if (logout!=null){
        session.setAttribute("loginId",0);
        session.setAttribute("loginName","");
    }

} 

%>

  
<!-- Modal con form login -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Identificación de usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" method="POST">
        <div class="modal-body">
                <input name="loginname" type="text" class="form-control"  placeholder="email">
                <input name="loginpassword" type="password" class="form-control" placeholder="password">
                <input type="hidden" name="loginpost" value="modal">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </form>
    </div>
  </div>
</div>