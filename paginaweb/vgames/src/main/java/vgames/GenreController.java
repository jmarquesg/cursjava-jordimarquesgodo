package vgames;

import java.util.ArrayList;
import java.util.List;

import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class GenreController {

    // getAll devuelve todos los registros de la tabla
    public static List<Genre> getAll(){
        List<Genre> listaGenres = new ArrayList<Genre>();
        String sql = "select * from Genre";

        try (Connection conn = DBConn.getConn();
             Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Genre u = new Genre(
                        rs.getInt("id"),
                        rs.getString("genre")
                );
                listaGenres.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaGenres;

    }

}
