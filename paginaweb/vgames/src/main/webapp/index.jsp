<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="vgames.*" %>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th >#</th>
      <th >Nombre</th>
      </tr>
  </thead>
  <tbody>

   <% for (Genre gen : GenreController.getAll()) { %>
        <tr>
        <td><%= gen.id_genre %></td>
        <td><%= gen.genre %></td>
        </tr>
    <% } %>

  </tbody>
</table>


</body>
</html>