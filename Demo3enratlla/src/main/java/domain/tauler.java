package main.java.domain;

/**
 * @author Jordi
 *
 */
public class tauler {
    int[] taula;


    public tauler(){
        this.taula = new int[9];
    }
    public tauler(int[] actual){this.taula = actual.clone();}
    public int[] getTaula() {
        return taula;
    }

    public void moviment(int jugador, int posicio){
        this.taula[posicio] = jugador;
    }

    /**
     * @return torna el numero de jugador guanyador, o -1 si ningun guanya
     */
    public int guanya(){
        int i = 0;
        int r = -1;
        //Comproba diagonals
        if(taula[0] != 0 && taula[0] == taula[4] && taula[0] == taula[8]) r = taula[0];
        if(taula[2] != 0 && taula[2] == taula[4] && taula[2] == taula[6]) r = taula[2];

        do{

            //Comproba si la fila es victoria
            if (taula[3*i] != 0 && taula[3*i] == taula[3*i +1] && taula [3*i] == taula[3*i +2]){
                r = taula[3*i];
            }

            //Comproba si columna es victoria

            if (taula[i] != 0 && taula[i] == taula[3+i] && taula[i] == taula[6+i]){
                r = taula[i];
            }


        } while(r == -1 && ++i < 3 );
        return r;
    }

    public void mostrartauler(){
        System.out.println("XXXXX");
        for( int i = 0; i< 3; i++){
            System.out.print("X");
            for( int j = 0; j<3; j++ ){
                if(taula[3*i+j] == 1)System.out.print("/");
                if(taula[3*i+j] == 2)System.out.print("O");
                if(taula[3*i+j] == 0)System.out.print(".");

            }
            System.out.println("X");
        }
        System.out.println("XXXXX");
    }

}
