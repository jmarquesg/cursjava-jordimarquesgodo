package main.java.domain;

public class Partidaweb {
    tauler taulajoc;

    public Partidaweb(String codi){
        System.out.println(codi);
      int[] actual= new int[9];
      for (int i = 0;i <9; i++){
        actual[i] = Character.getNumericValue(codi.charAt(i));
      }
      taulajoc = new tauler(actual);
    }

    public String ordinador(){
        maquina jmaquina = new maquina(2);
        int ronda = contalaronda();
        int move = jmaquina.juga(taulajoc,ronda);
        System.out.println(move);
        taulajoc.moviment(2,move);
        return codifica();
    }

    public int victoria(){
        return taulajoc.guanya();
    }

    private int contalaronda(){
        int[] actual = taulajoc.getTaula();
        int r= 0;
        for (int i = 0; i<9;i++){
            if (actual[i]!= 0) r++;
        }
        return r;
    }

    private String codifica(){
        int[] actual = taulajoc.getTaula();
        String r= "";
        for (int i = 0; i<9;i++){
            r += (Integer.toString(actual[i]));
        }
        return r;
    }

}
