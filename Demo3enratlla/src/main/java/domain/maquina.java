package main.java.domain;


public class maquina extends jugador {
    private int[] valor;
    private int[] tauleta;
    private int[] espais;
    public maquina(int tipo){
        super(tipo);
        valor = new int[]{20,10,20,10,30,10,20,10,20}; //suma = 8.0
    }



    public int juga(tauler tauler, int ronda) {
        this.tauleta = tauler.taula.clone();
        int r = 0;
        espais = new int[9-ronda];
        movdispo();

        int max = -9999;
        for (int i = 0; i < espais.length; i++){
            int[] tau = this.tauleta.clone();
            tau[espais[i]] = super.tipo;
            int[] esp = espais.clone();
            removeElement(esp,i);
            int aux = minimax(esp,tau, false,3);
            if (max < aux) {
                max = aux;
                r = espais[i];
            }
        }
        return r;
    }



 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void movdispo(){
        int j = 0;
        for( int i = 0; i< 9; i++){
            if(this.tauleta[i]==0){
                espais[j] = i;
                j++;
            }
        }
    }

    private int ple(int[] tau){
        int j = 0;
        for( int i = 0; i< 9; i++){
            if(tau[i]==0){
                j++;
            }
        }
        return j;
    }
    private int guanya(int[] tau){
        int i = 0;
        int r = -1;
        //Comproba diagonals
        if(tau[0] != 0 && tau[0] == tau[4] && tau[0] == tau[8]) r = tau[0];
        if(tau[2] != 0 && tau[2] == tau[4] && tau[2] == tau[6]) r = tau[2];
        while(r == -1 && i < 3 ){
            //Comproba si la fila es victoria
            if (tau[3*i] != 0 && tau[3*i] == tau[3*i +1] && tau [3*i] == tau[3*i +2]){
                r = tau[3*i];
            }
            if (tau[i] != 0 && tau[i] == tau[3+i] && tau[i] == tau[6+i]){
                r = tau[i];
            }
            i++;

        }
        if (r == super.tipo) {
            return 10;
        }
        if (r == (super.tipo%2)+1) return -10;
        return 0;
    }

    private int minimax(int[] dispo,int[] taul, boolean maximize,int d){
        int value = guanya(taul);
        if (value != 0 || d == 0) return value;
        if (maximize){
            value = -9999;
            for (int i = 0; i< dispo.length;i++){
                int[] dis = dispo.clone();
                removeElement(dis,i);
                int[] auxtau = taul.clone();
                auxtau[dispo[i]]=super.tipo;
                int daux = d -1;
                value = max(value,minimax(dis,auxtau,false,daux));
            }
        }
        else{
            value = 9999;
            for (int i = 0; i< dispo.length;i++){
                int[] dis = dispo.clone();
                removeElement(dis,i);
                int[] auxtau = taul.clone();
                auxtau[dispo[i]]=(super.tipo%2)+1;
                int daux = d -1;
                value = min(value,minimax(dis,auxtau,true,daux));
            }
        }
        return value;
    }

    private int min(int a, int b){
        if (a > b){
            return b;
        }
        else{
            return a;
        }
    }
    private int max(int a, int b){
        if (a < b){
            return b;
        }
        else{
            return a;
        }
    }

    private void removeElement(int[] arr, int removedIdx) {
        System.arraycopy(arr, removedIdx + 1, arr, removedIdx, arr.length - 1 - removedIdx);
    }

}
