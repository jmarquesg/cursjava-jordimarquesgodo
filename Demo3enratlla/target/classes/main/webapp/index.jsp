<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="main.java.domain.Partidaweb" %>
<%
  Partidaweb joc = new Partidaweb();
%>

<!DOCTYPE html>
<html lang="es-ES">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title>Java demo</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <style media="screen">
      .table{
        width: auto;
      }
      .far {
        font-size: 200px;
      }
      .fas {
        font-size: 200px;
      }
    </style>
  </head>
  <body>

    <div class="container align-center">
      <div class="row">
          <div class="col-2">

          </div>
          <div class="col">
            <table class="table table-borderless">
              <tbody>
                <tr>
                  <td><i class="far fa-square" id="0"></i></td>
                  <td><i class="far fa-square" id="1"></i></td>
                  <td><i class="far fa-square" id="2"></i></td>
                </tr>
                <tr>
                  <td><i class="far fa-square" id="3"></i></td>
                  <td><i class="far fa-square" id="4"></i></td>
                  <td><i class="far fa-square" id="5"></i></td>
                </tr>
                <tr>
                  <td><i class="far fa-square" id="6"></i></td>
                  <td><i class="far fa-square" id="7"></i></td>
                  <td><i class="far fa-square" id="8"></i></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col-2">

          </div>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $(document).on("click","i",function(){
          $(this).toggleClass("far fa-square fas fa-times");
        });
      });
    </script>
  </body>
</html>
