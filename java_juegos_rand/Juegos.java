package java_juegos_rand;
import java.util.Random;
import java.util.Scanner;

public class Juegos {

    public static Jugador jugador1;
    public static Jugador jugador2;
    public static int partidas;
    public static Scanner keyboard = new Scanner(System.in);

    /**
     * pide nombre de jugador, 1 o 2
     * crea objeto "Jugador" y lo asigna a jugador1 o jugador2
     */
    public static void pideJugador(int numJugador){
        //pedimos nombre de jugador
        String pregunta = String.format("Nombre para el jugador %d: ", numJugador);
        System.out.println(pregunta);
        String nombre = keyboard.next();
        Jugador j = new Jugador(nombre);

        if (numJugador == 1) {
            Juegos.jugador1 = j;
        }else{
            Juegos.jugador2 = j;
        }
    }


    /**
     * muestra menu de juegos y pide opción
     */

     public static void menu(){
       System.out.println("*******************");
       System.out.println("JUEGOS DISPONIBLES:");
       System.out.println("*******************");
       System.out.println("1: Cara o Cruz");
       System.out.println("2: Papel Piedra Tijera");
       System.out.println("3: ");
       System.out.println("*******************");
       //pedimos opcion de juego, comprobando validez
       int opcion = 0;
       do {
           System.out.print("Introduce juego: ");
           opcion = keyboard.nextInt();
       } while (opcion<1 || opcion>2);

       switch (opcion) {
           case 1:
               Juegos.caraCruz();
               break;
           case 2:
               Juegos.piedrapapeltijera();
               break;
           default:
               break;
       }
   }

   /**
    * juego caraCruz
    */
   public static void caraCruz(){
       Juegos.pideJugador(1); //solo interviene un jugador en este juego
       Juegos.partidas = 5; //partidas por defecto en este juego

       System.out.println();
       System.out.println("CARA O CRUZ:");
       System.out.println("************");
       System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
       System.out.println();
       System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
       System.out.println();

       Random rnd = new Random();
       //bucle de partidas
       for(int i=0; i<Juegos.partidas; i++){
           System.out.print("Cara (C) o cruz(X) ?  ");
           //pedimos apuesta aunque no se utiliza
           String apuesta = keyboard.next();
           // sea cual sea la probabilidad es un 50%...
           // obtenemos un boolean aleatorio
           boolean ganador = rnd.nextBoolean();
           if (ganador) {
               System.out.println(" Has acertado!");
               Juegos.jugador1.ganadas++;
           } else {
               System.out.println(" Lo siento...");
           }
           Juegos.jugador1.partidas++;
       }
       //creamos string con el resumen final de juego y lo mostramos
       String resumen = String.format("Jugador %s, %d partidas, %d ganadas.",
                       Juegos.jugador1.nombre,
                       Juegos.jugador1.partidas,
                       Juegos.jugador1.ganadas);
       System.out.println(resumen);
   }


   public static void piedrapapeltijera(){
     Juegos.pideJugador(1);
     Juegos.partidas = 5;
     System.out.println("Vamo a jugar hdp!!");
     System.out.println("##################");
     Random rand = new Random();
     Scanner sc = new Scanner(System.in);
     for(int i = 0;i < Juegos.partidas; i++){
       System.out.print("Papel(0), Piedra(1) o Tijera(2) ");
       int pc = rand.nextInt(3);
       int huma = sc.nextInt();
       System.out.print("PC escull " + pc +" :");
       if (pc == huma) {
         System.out.println(" Empatat");
       } else
       if ((pc+1) == huma || (huma+1)%3 == pc){
         System.out.println(" Victoria");
         Juegos.jugador1.ganadas++;
       } else
       {
         System.out.println(" Derrota");
       }
        Juegos.jugador1.partidas++;
     }
     //creamos string con el resumen final de juego y lo mostramos
     String resumen = String.format("Jugador %s, %d partidas, %d ganadas.",
                     Juegos.jugador1.nombre,
                     Juegos.jugador1.partidas,
                     Juegos.jugador1.ganadas);
     System.out.println(resumen);
   }

}
