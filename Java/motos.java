import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class motos {
    public static void main(String[] args) {
        String filtre =  args[0];
        File fin= new File("motos.csv");
        //List<String> provincias = new ArrayList<String>();
        Set<String> motos = new TreeSet<String>();
        try (    InputStreamReader fr = new InputStreamReader( new FileInputStream(fin), "UTF8");
                BufferedReader br = new BufferedReader(fr);
                ) {
            String linea;
            do {
                linea = br.readLine();
                if (linea!=null) {
                    motos.add(linea);
                }
            } while (linea!=null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (FileWriter writer = new FileWriter(filtre + ".csv");
        ){
          for (String moto : motos) {
            if(moto.toLowerCase().contains(filtre.toLowerCase())){
              writer.append(moto);
              System.out.println(moto);
            }
          }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
