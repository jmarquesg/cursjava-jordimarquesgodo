package com.sjava.spring.javacademia;
import java.util.Optional;
import  org.springframework.beans.factory.annotation.Autowired;
import  org.springframework.stereotype.Controller;
import  org.springframework.ui.Model;
import  org.springframework.web.bind.annotation.GetMapping;
import  org.springframework.web.bind.annotation.PathVariable;
import  org.springframework.web.bind.annotation.RequestMapping;
import  org.springframework.web.bind.annotation.RequestParam;
import  org.springframework.web.bind.annotation.ResponseBody;
@Controller
@RequestMapping(path="/alumno")
public class AlumnoController {
​ @Autowired
    private AlumnoRepository alumnoRepository;
​ @GetMapping(path="/add")
    public String add(​
                      @RequestParam​ String nombre, @RequestParam String email,
                      @RequestParam String ciudad){
        Alumno al = new Alumno();
        al.setNombre(nombre);
        al.setEmail(email);
        al.setCiudad(ciudad);
        alumnoRepository.save(al);
        return "redirect:"+"/alumno/all";
    }
​ @GetMapping(path="/{id}")
    public @ResponseBody Optional<Alumno> getId(​ @PathVariable String id​ ){
        return alumnoRepository.findById(Long.parseLong(id));
    }
    @GetMapping(path="/id")
    public @ResponseBody Optional<Alumno> getIdParam(@RequestParam String id){
        return alumnoRepository.findById(Long.parseLong(id));
    }
    @GetMapping(path="/ciudad/{ciudad}")
    public @ResponseBody Iterable<Alumno> getByCiudad(@PathVariable String ciudad){
        return alumnoRepository.findByCiudad(ciudad);
    }
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Alumno> getAll(){
        return alumnoRepository.findAll();
    }
    @GetMapping("/nuevo")
    public String create(Model model) {
        model.addAttribute("titulo", "Creando alumno");
        return "alumno/create";
    }
}