package com.sjava.spring.javacademia;
import org.springframework.data.repository.CrudRepository;
public interface AlumnoRepository extends CrudRepository<Alumno, Long> {
    Iterable<Alumno> findByCiudad(String ciudad);
}