package com.sjava.spring.javacademia;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class HomeController {

  //Jabulin272

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("person", "Ricard");
        model.addAttribute("msg", "probando spring");
        return "hi";
    }

}