package com.sjava.spring.javacademia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavacademiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavacademiaApplication.class, args);
	}
}
